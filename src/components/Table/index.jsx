import React, { useState } from "react";
import { useTable, usePagination, useGlobalFilter, useAsyncDebounce, useSortBy } from "react-table";

function GlobalFilter({ preGlobalFilteredRows, globalFilter, setGlobalFilter, setLoading, placeholder }) {
    const count = preGlobalFilteredRows.length;
    const [value, setValue] = useState(globalFilter);
    const onChange = useAsyncDebounce((value) => {
        setGlobalFilter(value || undefined);
        setLoading(false);
    }, 200);

    return (
        <div className="seacrh-wrapper">
            <div className="position-relative">
                <input
                    value={value || ""}
                    className="form-control b-0"
                    onChange={(e) => {
                        setLoading(true);
                        setValue(e.target.value);
                        onChange(e.target.value);
                    }}
                    placeholder={placeholder || "Search...."}
                />
                <span className="input-group-append">
                    <button className="btn rounded" onClick={(e) => onChange(value)}>
                        <i className="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </div>
    );
}

const Table = ({ columns, data = [], searchPlaceholder, loading }) => {
    const [showloader, setShowLoader] = useState(false);

    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page,
        gotoPage,
        rows,
        preGlobalFilteredRows,
        setGlobalFilter,
        setPageSize,
        canNextPage,
        state: { pageSize, globalFilter, pageIndex },
    } = useTable(
        {
            columns,
            data: data,
            initialState: { ServiceRequest: 0, pageSize: 10, disableSortRemove: true },
            // defaultColumn, // Be sure to pass the defaultColumn option
            // filterTypes,
        },
        useGlobalFilter, // useGlobalFilter!
        useSortBy,
        usePagination
    );
    const renderTableData = (index, cell) => {
        return (
            <td data-label={cell.column?.Header} {...cell.getCellProps()}  className="px-6 py-4 whitespace-nowrap">
                {cell.render("Cell")}
            </td>
        );
    };

    const renderRow = (row) => {
        return (
            <tr {...row.getRowProps()}>
                {row.cells.map((cell, index) => {
                    return renderTableData(index, cell);
                })}
            </tr>
        );
    };

    const handlePageChange = (page) => {
        setShowLoader(true);
        setTimeout(() => {
            // gotoPage(page);
            setPageSize(pageSize + 10);
            setShowLoader(false);
        }, [500]);
    };

    return (
        <>
        <div className="mt-2 flex flex-col">
            <div className="-my-2 overflow-x-auto -mx-4 sm:-mx-6 lg:-mx-8">
                <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                    <div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                        <table className="min-w-full divide-y divide-gray-200" {...getTableProps()}>
                            <thead className="bg-gray-50">
                            {headerGroups.map((headerGroup) => (
                                <tr {...headerGroup.getHeaderGroupProps()} className="hidden sm:table-row">
                                    {headerGroup.headers.map((column, i) => (
                                        <th
                                            scope="col"
                                            className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                                            {...column.getHeaderProps(column.getSortByToggleProps())}
                                        >
                                            {column.render("Header")}
                                            {!column.disableSortBy && (
                                                <span className="caret-container">
                                                    {column.isSorted ? (
                                                        column.isSortedDesc ? (
                                                            <span className="caret down" />
                                                        ) : (
                                                            <span className="caret up" />
                                                        )
                                                    ) : (
                                                        <>
                                                            <span className="caret up" />
                                                            <span className="caret down" />
                                                        </>
                                                    )}
                                                </span>
                                            )}
                                        </th>
                                    ))}
                                </tr>
                            ))}
                            </thead>
                            <tbody {...getTableBodyProps()} className="bg-white divide-y divide-gray-200">
                            {loading || showloader ? (
                                <></>
                                // <TableSkeleton pageSize={pageSize} columns={columns} />
                            ) : (
                                page.length > 0 &&
                                page.map((row, i) => {
                                    prepareRow(row);
                                    return renderRow(row);
                                })
                            )}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        {page.length > 9 && canNextPage && (
            <div className="w-full text-center my-[30px]">
                <button
                    className="py-0 text-BodyTextColor !text-base font-normal"
                    onClick={() => handlePageChange(pageIndex + 1)}
                >
                    Load More
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-4 w-4 ml-2.5 text-BodyTextColor"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                        stroke-width="2"
                    >
                        <path stroke-linecap="round" stroke-linejoin="round" d="M19 9l-7 7-7-7" />
                    </svg>
                </button>
            </div>
        )}
</>
);
};
export default Table;
