import { useState } from 'react';
import { loginFields } from "../constants/formFields";
import FormAction from "./FormAction";
import Input from "./Input";
import {useMutation} from "@apollo/client";
import {gqlTemplate} from "../services/resolvers";
import { useRoutes } from 'react-router'
import {useStoreActions} from "easy-peasy";
import {toast} from "react-toast";

const fields=loginFields;
let fieldsState = {};
fields.forEach(field=>fieldsState[field.id]='');

export default function Login(){

    // useRoutes.push("/");

    const { setUser } = useStoreActions((store) => store.user);

    const [LoginWithEmail, { loading: loginEmailLoading }] = useMutation(
        gqlTemplate.Mutation.GraphQL.signin,
        {
            context: { clientName: "graphql" },
            onCompleted: (data) => {
                if (data?.login) {
                    toast.success("Login SuccessFull")
                    setUser(data?.login);
                    window.location.reload(false);
                }
            },
            onError: (error) => console.log(error),
        }
    );

    const [loginState,setLoginState]=useState(fieldsState);

    const handleChange=(e)=>{
        setLoginState({...loginState,[e.target.id]:e.target.value})
    }

    const handleSubmit=(e)=>{
        e.preventDefault();
        authenticateUser();
    }

    //Handle Login API Integration here
    const authenticateUser = () =>{

        LoginWithEmail({variables:{input: {username: loginState?.username, password: loginState?.password}}})
    }

    return(
        <form className="mt-8 space-y-6" onSubmit={handleSubmit}>
            <div className="-space-y-px">
                {
                    fields.map(field=>
                        <Input
                            key={field.id}
                            handleChange={handleChange}
                            value={loginState[field.id]}
                            labelText={field.labelText}
                            labelFor={field.labelFor}
                            id={field.id}
                            name={field.name}
                            type={field.type}
                            isRequired={field.isRequired}
                            placeholder={field.placeholder}
                        />

                    )
                }
            </div>
            <FormAction handleSubmit={handleSubmit} loginEmailLoading = {loginEmailLoading} text="Login"/>

        </form>
    )
}
