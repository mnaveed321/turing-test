import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import {StoreProvider,} from "easy-peasy";
import store from "./store";
import MainPage from "./pages/mainPage";
import {ToastContainer} from "react-toast";
import React from "react";


function App() {

  return (
      <StoreProvider store={store}>
        <div className="App">
          <ToastContainer
              position="top-right"
              theme="colored"
              autoClose={1000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
          />
          <div className="min-h-full h-screen flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8">
            <div className="max-w-md w-full space-y-8">
              <BrowserRouter>
                <Routes>
                  <Route path="/" element={<MainPage/>} />
                </Routes>
              </BrowserRouter>
            </div>
          </div>
        </div>
      </StoreProvider>
  );
}

export default App;
