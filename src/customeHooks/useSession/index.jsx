import {useEffect, useState} from "react";
import {useStoreActions} from "easy-peasy";
import {useMutation} from "@apollo/client";
import {getToken, gqlTemplate} from "../../services/resolvers";

const useSession = () => {
    const { setUser } = useStoreActions((store) => store.user);

    const [refreshToken, { loading: loading }] = useMutation(
        gqlTemplate.Mutation.GraphQL.signin,
        {
            context: { clientName: "graphql" },
            onCompleted: (data) => {
                if (data?.login) {
                    setUser(data?.login);
                }
            },
            onError: (error) => console.log(error),
        }
    );

    useEffect(() => {
        refreshToken();
    },[])

}

export default useSession;