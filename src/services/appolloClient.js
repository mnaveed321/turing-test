import { useMemo } from "react";
import { InMemoryCache, ApolloClient, HttpLink, ApolloLink } from "@apollo/client";
import { onError } from "@apollo/client/link/error";

const getLink = (operation) => {
    const { clientName, token, venueSlug } = operation.getContext();
    let uri = 'https://frontend-test-api.aircall.io/graphql';
    const link = new HttpLink({
        uri,
    });

    let headers = {
        Origin: 'http://localhost:3000', // Envoirment url
    };

    if (token) {
        headers["Authorization"] = `Bearer ${token}`;
    }

    operation.setContext({
        headers,
    });

    return link.request(operation);
};

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors) {
        graphQLErrors.map(({ message, locations, path, code }) => {
            console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`);
            if (
                (message.includes("Authentication failed") && code === 401) ||
                message.includes("Unauthorized access")
            ) {
                if (window !== undefined) {
                    // signOut();
                    setTimeout(() => {
                        localStorage.clear();
                        window.location.href = window.location.pathname?.includes("checkout")
                            ? "/checkout"
                            : "/account/login";
                    }, [3000]);
                }
            } else {
               console.log(message);
            }
        });
    }

    if (networkError) {
        console.log(`[Network error]: ${networkError}`);
    }
});

// const fragmentMatcher = new IntrospectionFragmentMatcher({
//     introspectionQueryResultData,
// });

const createApolloClient = () => {
    return new ApolloClient({
        ssrMode: typeof window === "undefined",
        link: errorLink.concat(
            ApolloLink.split(
                () => true,
                (operation) => getLink(operation),
                errorLink
            )
        ),
        cache: new InMemoryCache(),
    });
};

export const initializeApollo = (initialState = null) => {
    const apolloClient = createApolloClient();

    if (initialState) {
        apolloClient.cache.restore(initialState);
    }

    return apolloClient;
};

export const useApollo = (initialState) => {
    const store = useMemo(() => initializeApollo(initialState), [initialState]);
    return store;
};
