import { gql } from "@apollo/client";

export const gqlTemplate = {
    Mutation: {
        GraphQL: {
            signin: gql`
                mutation login(
                    $input: LoginInput!
                ) {
                    login(
                        input: $input
                    ) {
                        access_token
                        refresh_token
                        user {
                            id
                            username
                        }
                    }
                }
            `,
            refreshTokenV2: gql`
                mutation refreshTokenV2{
                    refreshTokenV2{
                        access_token
                        refresh_token
                        user {
                            id
                            username
                        }
                    }
                }
            `,
            archiveCall: gql`
                mutation archiveCall($id: ID!) {
                    archiveCall(id: $id) {
                        direction
                        __typename
                    }
                }
            `,
            updateCall: gql`
                subscription updateCall($id: ID!) {
                    updateCall(id: $id) {
                        __typename
                    }
                }
            `,
        }
    },
    SUBSCRIPTION: {
        GraphQL: {
            updateCall_SUBSCRIPTION: gql`
                subscription onUpdatedCall($id: ID!) {
                    onUpdatedCall(id: $id) {
                        __typename
                    }
                }
            `,
        }
    },
    Query: {
        GraphQL: {
            readCalls: gql`
                query paginatedCalls {
                    paginatedCalls(offset: 0) {
                        nodes{
                            id
                            direction
                            from
                            to
                            duration
                            via
                            is_archived
                            call_type
                            created_at
                            notes{
                                id
                                content
                            }
                        }
                    }
                }
            `,
        },
    }
}



export const getToken = (refresh_token) => {
    return refresh_token;
};