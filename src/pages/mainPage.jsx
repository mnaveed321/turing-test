import LoginPage from "./Login";
import useSession from "../customeHooks/useSession";
import {useLazyQuery, useMutation, useSubscription} from "@apollo/client";
import {getToken, gqlTemplate} from "../services/resolvers";
import {useEffect, useState} from "react";
import {useStoreActions, useStoreState} from "easy-peasy";
import Table from "../components/Table";
import {toast} from "react-toast";
import {Link} from "react-router-dom";
import Loader from "../components/Loader";
import Button from "../components/Button";


const MainPage = () => {

    const {user} = useStoreState((state) => state.user);
    const { setUser } = useStoreActions((store) => store.user);


    const [renderPage, setRenderPage]= useState([]);
    const [callsData, setCallsData] = useState([]);


    const [readCalls, { data: callData, loading: readCallLoading, refetch: refetchReadCalls }] = useLazyQuery(
        gqlTemplate.Query.GraphQL.readCalls,
        {
            context: { token: getToken(user?.access_token) },
            onCompleted: (data) => {
                setCallsData(data?.paginatedCalls?.nodes);
            },
            onError: (error) => {
                toast.error("Token Expired Please Login Again")
                setRenderPage([])
            },
        }
    );

    const [archiveCall, { data: archiveCallData, loading: archiveCallLoading }] = useMutation(
        gqlTemplate.Mutation.GraphQL.archiveCall,
        {
            context: { token: getToken(user?.access_token) },
            onCompleted: (data) => {
                refetchReadCalls();
                toast.success("Action Succeeded");
            },
            onError: (error) => {
                toast.error(error.message);
                // setRenderPage([])
            },
        }
    );


    const [refreshToken, { loading: loading }] = useMutation(
        gqlTemplate.Mutation.GraphQL.refreshTokenV2,
        {
            context: { token: getToken(user?.refresh_token) },
            onCompleted: (data) => {
                if (data?.login) {
                    setUser(data?.login);
                    refetchReadCalls();
                }
            },
            onError: (error) => {
                setRenderPage([]);
            },
        }
    );


    useEffect(() => {
        refreshToken();
    },[])

    useEffect( () => {
        if(user){
            setRenderPage(user);
            readCalls();
        }
    },[user])

    const onHandleArchiveCall = (id) => {
        archiveCall({variables: {id: id}})
    }



    const columns = [
        {
            id: "ID",
            Header: "ID",
            // eslint-disable-next-line no-unused-expressions
            accessor: (row) => {row?.duration},
        },
        {
            id: "From",
            Header: "From",
            accessor: (row) => (row?.from),
        },
        {
            id: "To",
            Header: "To",
            accessor: (row) => (row?.to),
        },
        {
            id: "is_archived",
            Header: "is_archived",
            accessor: (row) => ( row?.is_archived ? "Archived" : "Not archived"),
        },
        {
            Header: "Make Archive Call",
            id: "details",
            accessor: (row) => (
                <Button className=" border border-transparent text-white bg-purple-600 py-2 px-2" onClick={() => onHandleArchiveCall(row?.id)}
                >
                    Archive Call
                </Button>
            ),
        },
    ];

   return (
       <>
           {renderPage?.user ? (
               <>
               {readCallLoading ? <Loader/> : (
                   <div className="grid grid-rows-1">
                       <h1 className="text-[10px] text-black mb-10 pb-2.5 border-b-[1px] border-b-gray-BorderGrey font-bold capitalize lg:text-5xl hidden md:block">
                          My Calls
                       </h1>
                       <Table columns={columns} data={callsData} loading={readCallLoading} />

                   </div>
                   )}
               </>
           ) : <LoginPage/>}
       </>
   )
}

export default MainPage;