import { createStore, persist } from "easy-peasy";
import user from "./user";

const modal = persist(
    {
        user: user,
    },
    {
        whitelist: ["setting"],
        storage: "localStorage",
    }
);

const middleware = [];


const config = {
    middleware,
};

const store = createStore(modal, config);

export default store;
