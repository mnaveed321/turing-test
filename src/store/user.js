import { action, computed, thunk } from "easy-peasy";

const user = {
    user: {},
    /* Actions */
    setUser: action((state, data) => {
        state.user = data;
    }),
};

export default user;
